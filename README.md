# Bypass Paywalls Clean

Extension allows you to read articles from (supported) sites that implement a paywall.

You can also add a domain as custom site and try to bypass the paywall.  
Weekly updates are released for fixes and new sites.

Chrome: [https://gitflic.ru/project/magnolia1234/bypass-paywalls-chrome-clean](https://gitflic.ru/project/magnolia1234/bypass-paywalls-chrome-clean)

Firefox: [https://gitflic.ru/project/magnolia1234/bypass-paywalls-firefox-clean](https://gitflic.ru/project/magnolia1234/bypass-paywalls-firefox-clean)

Adblocker filter (& userscripts): [https://gitflic.ru/project/magnolia1234/bypass-paywalls-clean-filters](https://gitflic.ru/project/magnolia1234/bypass-paywalls-clean-filters)

PS GitFlic only has Russian interface (use like Google Translate).
